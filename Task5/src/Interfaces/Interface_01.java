package Interfaces;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Interface_01 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JButton button = new JButton("Click Here");
        frame.setLayout(new BorderLayout());
        frame.add(button, BorderLayout.CENTER);

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Says: " +
            event.getActionCommand());
            }
        });
	
	frame.pack();
        frame.setVisible(true);
    }
}